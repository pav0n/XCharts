﻿using System.Collections.Generic;
using System.Drawing;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Ctrl = charts.Controls;
using iOSRenderer = charts.iOS.Renderer;
using iChart = iOSCharts;
[assembly: ExportRenderer(typeof(Ctrl.BarChartView), typeof(iOSRenderer.BarChartViewRenderer))]
namespace charts.iOS.Renderer
{
	public class BarChartViewRenderer : ViewRenderer<Ctrl.BarChartView, iChart.BarChartView>
	{

		public BarChartViewRenderer()
		{
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if(e.PropertyName == Ctrl.BarChartView.DataProperty.PropertyName){
				
			}
		}

		private iChart.BarChartData GetData(){
			var data = Element.Data;
			var dataSets = data.DataSets;
			return null;
		}
		protected override void OnElementChanged(ElementChangedEventArgs<Ctrl.BarChartView> e)
		{

			if (e.NewElement != null)
			{
				if (Control == null)
				{
					SetNativeControl(new iChart.BarChartView(RectangleF.Empty));
				}
			}

			Control.ChartDescription.Enabled = true;
			Control.DrawGridBackgroundEnabled = true;
			Control.DragEnabled = true;
			Control.SetScaleEnabled(true);
			Control.PinchZoomEnabled = false;
			iChart.ChartYAxis LeftAxis = Control.LeftAxis;
			iChart.ChartXAxis XAxis = Control.XAxis;
			XAxis.LabelPosition = iOSCharts.XAxisLabelPosition.Bottom;
			Control.RightAxis.Enabled = false;
			Control.Data = GetBarData();

			base.OnElementChanged(e);
		}



		private iChart.BarChartData GetBarData()
		{
			List<float> valueList = new List<float>(){
				1,2,3,4,5,6,7,8
			};

			iChart.BarChartDataEntry[] entries = new iChart.BarChartDataEntry[valueList.Count];
			for (int i = 0; i < valueList.Count; i++)
			{
				entries[i] = new iChart.BarChartDataEntry(valueList[i], i);
			}

			//barChart.MoveViewToX(valueList.Count)
			iChart.BarChartDataSet barDataSet = new iChart.BarChartDataSet(entries, "Vital Reading");

			string[] dayList = {"1","2","3","4","5","6","7","8"};

			iChart.IInterfaceBarChartDataSet[] dataList = new iChart.IInterfaceBarChartDataSet[1];
			dataList[0] = barDataSet;
			iChart.BarChartData barData = new iChart.BarChartData(dataList);
			//barDataSet.SetColors(colorList);

			return barData;
		}


	}
}
