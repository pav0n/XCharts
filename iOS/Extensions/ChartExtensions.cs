﻿using System;
using System.Collections.Generic;
using iChart = iOSCharts;
using Ctrl = charts.Controls;
using Foundation;
using System.Linq;

namespace charts.iOS.Extensions
{
	public static class ChartExtensions
	{
		public static iChart.BarChartDataEntry[] ToNativeEntries(this IList<Ctrl.ChartDataEntry> entries){
			iChart.BarChartDataEntry[] nativeEntries = new iChart.BarChartDataEntry[entries.Count];
			for (int i = 0; i<entries.Count; i++)
			{
				var entry = entries[i];
				nativeEntries[i] = new iChart.BarChartDataEntry(
					entry.X,
					entry.Y
				);
			}
			return nativeEntries;
		}

		public static iChart.BarChartData ToNativeData(this Ctrl.BarChartData data){
			var DataSets = data.DataSets;
			IList<iChart.IInterfaceBarChartDataSet> dataList = new List<iChart.IInterfaceBarChartDataSet>();
			foreach (var dataSet in DataSets)
			{
				var nativeEntries = dataSet.Values.ToNativeEntries();
				iChart.BarChartDataSet barDataSet = new iChart.BarChartDataSet(nativeEntries, dataSet.Label);
				dataList.Add(barDataSet);
			}
			iChart.BarChartData barData = new iChart.BarChartData(dataList.ToArray());
			return barData;
		}
	}
}
