﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
namespace charts.Controls
{
	public interface IBarChartView{
		BarChartData Data { set; get; }
	}
	public class BarChartView : View, IElementConfiguration<BarChartView>
	{
		public static readonly BindableProperty DataProperty = BindableProperty.Create("Data", typeof(BarChartData), typeof(BarChartView), default(BarChartData));
		public BarChartData Data 
		{ 
			get { return (BarChartData)GetValue(DataProperty);} 
			set { SetValue(DataProperty, value);} 
		}
		public IPlatformElementConfiguration<T, BarChartView> On<T>() where T : IConfigPlatform
		{
			throw new NotImplementedException();
		}

	}

	public class ChartDataEntryBase
	{
		public double Y;
		public object Data;
		public ImageSource icon;
	}

	public class ChartDataEntry:ChartDataEntryBase{
		public double X;
	}

	public class ChartDataSet{
		public double YMin;
		public double YMax;
		public double XMin;
		public double XMax;
		private IList<ChartDataEntry> _values;
		public string Label;
		public bool AddEntry(ChartDataEntry entry)
		{
			if(_values == null)
			{
				_values = new List<ChartDataEntry>();
			}
			_values.Add(entry);
			return true;
		}
		public IList<ChartDataEntry> Values
		{
			get { return _values;}
		}

		public ChartDataSet( IList<ChartDataEntry> values, string label){
			_values = values;
			Label = label;
		}
	}


	public class BarChartData
	{
		public readonly IList<ChartDataSet> DataSets;
		public double BarWidth = 0.85;
		public BarChartData(IList<ChartDataSet> DataSets)
		{
			this.DataSets = DataSets;
		}
	}


}
